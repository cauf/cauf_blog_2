from django.test import TestCase
from markdown import Markdown
from cauf_blog_2.settings import (
    MARKDOWN_EXTENSIONS, 
    MARKDOWN_EXTENSIONS_CONFIG,
)
from blog.models import Article, Category


class CreateArticleCase(TestCase):
    def _set_category(self):
        self.category = Category(
            caption="Проверка",
        )
        self.category.save()

    def setUp(self) -> None:
        self._set_category()
        self.article = Article(
            caption='test_article',
            preview="**Проверка** __разметки__ превью",
            text=(
                "**Проверка** __разметки__ markdown\n\n"
                "[TOC]\n\n"
                "# Заголовок 1\n\n"
                "* список 1\n"
                "\t* список 11\n"
                "\t\t* список 111\n"
                "\tСписок 12\n\n"
                "## заголовок 2"
            ),
            category=self.category,
        )
        # print(self.article.text_md)
        self.article.save()

    def test_category(self):
        self.assertEqual(self.category.pk, 1)

    def test_edit_dt(self):
        self.assertNotEqual(self.article.edit_dt, None)
