from django.shortcuts import render, redirect, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from blog.models import (
    Article,
    Category,
)
from blog.forms import (
    ArticleForm,
    CategoryForm,
)


def home(request):
    return redirect('blog:all_articles')


class ArticleListView(ListView):
    template_name = 'all_articles.html'
    model = Article


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'article.html'


class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    form_class = ArticleForm
    template_name = 'article_update.html'
    # login_url = reverse('login')


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = Article
    form_class = ArticleForm
    template_name = 'article_update.html'


class CategoryListView(ListView):
    template_name = 'all_categories.html'
    model = Category


class CategoryDetailView(DetailView):
    model = Category
    template_name = 'category.html'


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'category_update.html'
    
    def get_success_url(self):
        return reverse('blog:category', args=(self.object.pk,))


class CategoryUpdateView(UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'category_update.html'

    def get_success_url(self):
        return reverse('blog:category', args=(self.object.pk,))


class CategoryDeleteView(LoginRequiredMixin, DeleteView):
    model = Category

    def get_success_url(self):
        return reverse('blog:all_categories')
