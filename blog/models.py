from django.db import models
from django.urls import reverse
from datetime import datetime
from pytz import timezone
from markdown import markdown, Markdown
from cauf_blog_2.settings import (
    MARKDOWN_EXTENSIONS, 
    MARKDOWN_EXTENSIONS_CONFIG,
    TIME_ZONE,
)


class Category(models.Model):
    caption = models.CharField('название', max_length=50)
    description = models.TextField('описание', blank=True, default='')
    parent = models.ForeignKey(
        'self', 
        on_delete=models.CASCADE,
        related_name='childs', 
        verbose_name='родитель',
        blank=True, 
        null=True,
    )

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'категории'

    def __repr__(self):
        return f'<Category {self.id}:"{self.caption}">'

    def __str__(self):
        return self.caption


class NormalTag(models.Model):
    text = models.CharField('текст', max_length=255)

    class Meta:
        verbose_name = 'нормализованный тег'
        verbose_name_plural = 'нормализованные теги'

    def __repr__(self):
        return f'<NormalTag {self.id}:"{self.text}">'

    def __str__(self):
        return self.text


class Tag(models.Model):
    text = models.CharField('текст', max_length=255)
    normal_tag = models.ForeignKey(
        NormalTag, 
        on_delete=models.CASCADE, 
        related_name='tags',
        verbose_name='нормализованный тег',
    )

    class Meta:
        verbose_name = 'тег'
        verbose_name_plural = 'теги'

    def __repr__(self):
        return f'<Tag {self.id}:"{self.text}">'

    def __str__(self):
        return self.text


class Article(models.Model):
    caption = models.CharField('название', max_length=255)
    preview = models.TextField('разметка описания', blank=True, default='')
    text = models.TextField('разметка текста')
    edit_dt = models.DateTimeField('дата правки', blank=True, null=True)
    shows_count = models.IntegerField(
        'количество показов', 
        blank=True, 
        default=0, 
    )
    create_dt = models.DateTimeField('дата создания', auto_now=True)
    public_dt = models.DateTimeField('дата публикации', default=datetime.now())
    tags = models.ManyToManyField(
        Tag,
        related_name='articles',
        verbose_name='теги',
        blank=True,
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='articles',
        verbose_name='категория',
    )

    class Meta:
        verbose_name = 'статья'
        verbose_name_plural = 'статьи'

    def __repr__(self):
        print_caption = (
            f'{self.caption[:20]}...'
            if len(self.caption) > 20
            else self.caption
        )
        return f'<Article {self.id}:{print_caption!r}>'

    def __str__(self):
        return self.caption

    def save(self, *args, **kwargs):
        tz = timezone(TIME_ZONE)
        self.edit_dt = datetime.now(tz=tz)
        super(Article, self).save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('blog:article', kwargs={'pk':self.pk})
