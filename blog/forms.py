from django.forms import (
    ModelForm, 
    CharField, 
    Form,
    ImageField,
)
from django.forms.widgets import TextInput
from blog.models import (
    Article,
    Category,
)


class ArticleForm(ModelForm):
    tags = CharField(max_length=255, label="Теги")
    class Meta:
        model = Article
        fields = [
            'caption', 
            'preview', 
            'text', 
            'public_dt', 
            'tags',
            'category',
        ]
        localized_fields = '__all__'


class ImageForm(Form):
    image_file = ImageField(
        allow_empty_file=False, 
        required=True,
    )


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = (
            'caption',
            'description',
            'parent',
        )
        localized_fields = '__all__'
