from django import template
from django.template.defaultfilters import stringfilter
from markdown import markdown as call_md
from cauf_blog_2.settings import (
    MARKDOWN_EXTENSIONS,
    MARKDOWN_EXTENSIONS_CONFIG,
)


register = template.Library()


@register.filter
@stringfilter
def markdown(value):
    return call_md(
        value, 
        extensions=MARKDOWN_EXTENSIONS, 
        ex=MARKDOWN_EXTENSIONS_CONFIG,
    )
