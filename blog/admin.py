from django.contrib import admin
from blog.models import (
    Category,
    NormalTag,
    Tag,
    Article,
)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


class TagTabular(admin.TabularInline):
    model = Tag


@admin.register(NormalTag)
class NormalTagAdmin(admin.ModelAdmin):
    inlines = [TagTabular]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    fields = (
        'caption', 
        'preview', 
        'text', 
        'public_dt', 
        'tags', 
        'category',
    )
