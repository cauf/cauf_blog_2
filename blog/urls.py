from django.urls import path
from blog.views import (
    ArticleListView,
    ArticleCreateView,
    ArticleDetailView,
    ArticleUpdateView,
    CategoryListView,
    CategoryCreateView,
    CategoryDetailView,
    CategoryUpdateView,
    CategoryDeleteView,
)


app_name = 'blog'


urlpatterns = [
    # Articles
    path('', ArticleListView.as_view(), name='all_articles'),
    path('new', ArticleCreateView.as_view(), name='new_article'),
    path('<int:pk>', ArticleDetailView.as_view(), name='article'),
    path('<int:pk>/update', ArticleUpdateView.as_view(), name='update_article'),
    # Categories
    path('categories', CategoryListView.as_view(), name='all_categories'),
    path('categories/new', CategoryCreateView.as_view(), name='new_category'),
    path('categories/<int:pk>', CategoryDetailView.as_view(), name='category'),
    path('categories/<int:pk>/update', CategoryUpdateView.as_view(), name='update_category'),
    path('categories/<int:pk>/delete', CategoryDeleteView.as_view(), name='delete_category'),
]   