from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            nxt = request.POST.get('next')
            if nxt:
                return redirect(nxt)
            return redirect('blog:all_articles')
        return render(request, 'login.html', {
            'error': 'Неправильный логин или пароль',
        })
    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('blog:all_articles')